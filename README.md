# README #

### Project summary ###

The app runs in vagrant on PostgreSQL database.
When powered up, Vagrant creates a private network on IP 55.55.55.5 which allows the django ap to be accessed from a browser on host ( 55.55.55.5:8000 )
if launched with "python manage.py runserver 0.0.0.0"

### How it works ###
You get a list with each article and it's version. If the article is not published, you can edit the body thus creating a new version on it.

Missing features : login and role management, editing all items on an article.

### Alternative version ###
- A REST service based on tastypie and django-simple-history with a proper UI in Bootstrap and Angular.