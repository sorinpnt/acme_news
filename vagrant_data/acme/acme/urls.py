from django.conf.urls import include, url
from django.contrib import admin

from acme_articles.models import Category
from acme_articles.models import Article
from acme_articles.models import ArticleVersion

from acme_articles.views import ArticleView
from acme_articles.views import ArticleDetails
from acme_articles.views import ArticleEdit


admin.site.register(Category)
admin.site.register(Article)
admin.site.register(ArticleVersion)

urlpatterns = [
    url(r'^article/list', ArticleView.as_view(), name="article_list"),
    url(r'^article/edit', ArticleEdit.as_view(), name="article_list"),
    url(r'^article/', ArticleDetails.as_view(), name="article_details"),
    url(r'^admin/', include(admin.site.urls)),
]
