from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

class Category(models.Model):
    category_name = models.CharField(max_length=50)

    def __str__(self):
        return self.category_name


class Article(models.Model):
    category = models.ForeignKey(Category)
    submitter = models.CharField(max_length=100)
    published = models.BooleanField(default=False)
    published_by = models.CharField(max_length=100, blank=True, null=True)
    publishing_date = models.DateField(null=True, blank=True)


class ArticleVersion(models.Model):
    attached_article = models.ForeignKey(Article)
    version = models.PositiveIntegerField(default=0)
    title = models.CharField(max_length=900)
    body = models.TextField()
    creating_user = models.ForeignKey(User)
    creating_date = models.DateTimeField(default=datetime.now, blank=True)
