from django.views.generic import View
from django.shortcuts import render_to_response
from models import Article
from models import ArticleVersion
from copy import deepcopy
from django.http import HttpResponseRedirect
from django.template import RequestContext
from datetime import datetime


class ArticleView(View):
    def get(self, request):
        articles = Article.objects.all()
        for article in articles:
            article.versions = article.articleversion_set.all()
        return render_to_response(
            'article_list.html',
            {
                'article_list': articles
            }
        )


class ArticleDetails(View):
    def get(self, request):
        article_id = int(request.GET.get('id', -1))
        article_version = int(request.GET.get('v', -1))
        article = ArticleVersion.objects.get(
            version=article_version,
            attached_article=article_id
        )
        return render_to_response(
            'article_details.html',
            {
                'article': article
            }
        )


class ArticleEdit(View):
    def get(self, request):
        article_id = int(request.GET.get('id', -1))
        article = ArticleVersion.objects.filter(
            attached_article=article_id
        ).latest('creating_date')
        return render_to_response(
            'article_edit.html',
            {
                'article': article
            },
            RequestContext(request)
        )

    def post(self, request):
        article_id = int(request.POST.get('version_id', -1))
        article_version = ArticleVersion.objects.get(id=article_id)
        new_version = deepcopy(article_version)
        new_version.version = int(new_version.version) + 1
        new_version.id = None
        new_version.body = request.POST.get("body", "")
        new_version.creating_date = datetime.now()
        new_version.save()
        return HttpResponseRedirect("/article/list/")
