# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('submitter', models.CharField(max_length=100)),
                ('published', models.BooleanField(default=False)),
                ('published_by', models.CharField(max_length=100)),
                ('publishing_date', models.DateField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='ArticleVersion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('version', models.PositiveIntegerField(default=0)),
                ('title', models.CharField(max_length=900)),
                ('body', models.TextField()),
                ('creating_date', models.DateField(blank=True)),
                ('creating_time', models.TimeField(blank=True)),
                ('attached_article', models.ForeignKey(to='acme_articles.Article')),
                ('creating_user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category_name', models.CharField(max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='article',
            name='category',
            field=models.ForeignKey(to='acme_articles.Category'),
        ),
    ]
