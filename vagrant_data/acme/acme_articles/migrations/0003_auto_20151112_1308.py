# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('acme_articles', '0002_auto_20151112_1302'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='published_by',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='publishing_date',
            field=models.DateField(null=True, blank=True),
        ),
    ]
