# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('acme_articles', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='published_by',
            field=models.CharField(max_length=100, blank=True),
        ),
        migrations.AlterField(
            model_name='articleversion',
            name='creating_date',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='articleversion',
            name='creating_time',
            field=models.TimeField(),
        ),
    ]
