# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('acme_articles', '0003_auto_20151112_1308'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='articleversion',
            name='creating_time',
        ),
        migrations.AlterField(
            model_name='articleversion',
            name='creating_date',
            field=models.DateTimeField(default=datetime.datetime.now, blank=True),
        ),
    ]
